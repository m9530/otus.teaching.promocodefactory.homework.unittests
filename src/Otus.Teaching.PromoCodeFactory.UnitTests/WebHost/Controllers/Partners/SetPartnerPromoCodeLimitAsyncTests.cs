﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : IClassFixture<TestFixture>
    {
        private readonly PartnersController _partnersController;
        private readonly Mock<IRepository<Partner>> _partnerRepositoryMock;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _partnerRepositoryMock = new Mock<IRepository<Partner>>();
            _partnersController = new PartnersController(_partnerRepositoryMock.Object);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimit_IfPartnerNotFound_Returns404()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            _partnerRepositoryMock
                .Setup(x => x.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync((Partner) null);

            var request = new SetPartnerPromoCodeLimitRequest
            {
                EndDate = DateTime.Now.AddDays(1),
                Limit = 1
            };
            
            // Act
            var result =
                await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimit_IfPartnerBlocked_Returns400()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = PartnerBuilder();
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                PromoCodeLimitBuilder()
            };
            partner.IsActive = false;
            _partnerRepositoryMock
                .Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, 
                new SetPartnerPromoCodeLimitRequest());

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>().Which.StatusCode.Should().Be(400);
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimit_IfLimitIsSet_ResetPromoCodes()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = PartnerBuilder();
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                PromoCodeLimitBuilder()
            };
            
            _partnerRepositoryMock
                .Setup(repository => repository.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId,
                new SetPartnerPromoCodeLimitRequest { Limit = 1 });

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimit_IfLimitIsOver_NotResetPromoCodes()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = PartnerBuilder();
            var numberIssuedPromoCodes = 3;
            partner.NumberIssuedPromoCodes = numberIssuedPromoCodes;
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                PromoCodeLimitBuilder()
            };
            
            _partnerRepositoryMock
                .Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), 
                new SetPartnerPromoCodeLimitRequest());

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(3);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimit_IfSetLimitLessThanZero_Returns400()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = PartnerBuilder();
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                PromoCodeLimitBuilder()
            };
            _partnerRepositoryMock
                .Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId,
                new SetPartnerPromoCodeLimitRequest { Limit = -1 });

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>().Which.StatusCode.Should().Be(400);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimit_IfSetNewLimit_DataSavedInDb()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = PartnerBuilder();
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                PromoCodeLimitBuilder()
            };
            
            _partnerRepositoryMock
                .Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, 
                new SetPartnerPromoCodeLimitRequest
                {
                    
                    Limit = 3, 
                    EndDate = DateTime.Now
                });

            // Assert
            _partnerRepositoryMock.Verify(x=>x.UpdateAsync(It.IsAny<Partner>()), Times.Once());
        }

        
        private static Partner PartnerBuilder()
        {
            return new Partner
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true
            };
        }

        private static PartnerPromoCodeLimit PromoCodeLimitBuilder()
        {
            return new PartnerPromoCodeLimit
            {
                Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                CreateDate = new DateTime(2020, 07, 9),
                EndDate = new DateTime(2020, 10, 9),
                Limit = 100
            };
        }
    }
}